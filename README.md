# My Beautiful Scripts

Requirements: Linux

Original OS: Ubuntu 20.04

- How to include personalized scripts to your terminal

  1. Add the following line replacing the directory to your scripts' directory:
  ```
  SCRIPTS_DIR = /Dir/To/Your/Scripts
  ```

  2. Add the following lines to include your scripts in PATH.
  ```
  # >>> personalized functions >>>
  # Contents within this block has been directly written by the user
  if [ -d "$HOME/$SCRIPTS_DIR ] ; then
    PATH="$PATH:$HOME/$SCRIPTS_DIR
  fi
  ```

- I have more housekeeping scripts that I may include in the future. Will see...
